# Pi Picture Uploader
This is a project to turn a Raspberry Pi into an automatic webcam which uploads images
to a Dropbox folder and also stitches the day's images togehter into a timelapse
video.

## Automatic Webcam Setup

Create a cron job (`crontab -e`) for each of the scripts in this directory. You can have
it take pictures however frequently you'd like, and the Dropbox uploader can also run
however often you'd like, though it's nice to run it fairly frequently so it doesn't
have a massive queue of images to process.

## Timelapse Setup

1. Install `ffmpeg`:

https://www.vimp.com/en/web/faq-installation/items/how-to-install-the-transcoding-tools-on-debian-8-jessie.html

2. Use `ffmpeg`:

http://ubuntuforums.org/showthread.php?t=2022316

3. A nicer way to use `ffmpeg`:

https://trac.ffmpeg.org/wiki/Create%20a%20video%20slideshow%20from%20images

	ffmpeg -framerate 1/5 -i img%03d.png -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4
	ffmpeg -framerate 1/5 -i img%03d.png -c:v libx264 -vf fps=25 -pix_fmt yuv420p out.mp4

