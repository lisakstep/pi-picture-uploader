#!/bin/bash
sleep 40 # so as to not run while new pictures are being taken

FOLDER_NAME="/home/pi/Pictures/webcam/$(date +"%Y-%m-%d")"
mkdir -p $FOLDER_NAME

FILES=/home/pi/Pictures/webcam_staging/*
for f in $FILES
do
  /home/pi/Dropbox-Uploader/dropbox_uploader.sh upload $f /$(date +"%Y-%m-%d")/ && mv $f $FOLDER_NAME
done
